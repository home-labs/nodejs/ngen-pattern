import { InstanceState } from '@cyberjs.on/types';

import { Memento } from "./memento";


export interface Originator<T> {

    createMemento(): Memento<InstanceState<T>>;

    restoreState(memento: Memento<InstanceState<T>>): void;

}
