import { Iterable } from "./iterable";


export interface IteratorAggregate<V> {

    getIterator(): Iterable<V>;

}
