// Abstract Iterator
export interface Iterable<V> {

    [Symbol.iterator](): Iterable<V>;

    next(): { value?: V, done?: boolean };

}
