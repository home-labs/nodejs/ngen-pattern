import { Iterable } from "./iterable";


// Concrete Iterator
export class FlatIterator<V> implements Iterable<V> {

    protected index!: number;

    protected keys!: number[];

    protected lastIndex!: number;

    constructor(
        // the queue
        private collection: V[]
    ) {

        this.index = 0;
        this.keys = Array.from(collection.keys());
        this.lastIndex = this.keys.length - 1;

    }

    [Symbol.iterator](): Iterable<V> {
        return this;
    }

    next(): { value?: V, done?: boolean } {

        let data: { value?: V, done?: boolean } = {};

        let value: V;

        if (this.index <= this.lastIndex) {
            value = this.collection[this.key()];
            this.index++;
            data = { value: value };
        } else {
            data = { done: true };
        }

        return data;
    }

    protected key() {
        return this.keys[this.index];
    }

}
