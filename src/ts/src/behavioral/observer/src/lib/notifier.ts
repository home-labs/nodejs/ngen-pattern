// Abstract Subject
export interface Notifier {

    notify(...args: any[]): void;

}
