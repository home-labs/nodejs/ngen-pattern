import { AbstractNotifier } from "./abstractNotifier.js";


// Abstract Observer
export interface Subscriber {

    // this method is called internally, so there isn't matter its arbitraru number os arguments
    receiveNotification(...args: any[]): void;

    getNotifier(): AbstractNotifier;

}
