// abstract Prototype
export interface Clonable {

    getClone(): this;

}
